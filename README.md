# fizz-buzz

A FizzBuzz application, allowing a user to enter any number between 1 and 100 and the number gets evaluated as to whether it is a Fizz, Buzz or FizzBuzz.