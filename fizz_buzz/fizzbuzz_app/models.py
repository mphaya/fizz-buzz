# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class UserFizzBuzz(models.Model):
    user = models.ForeignKey(User)
    test_name = models.CharField(max_length=255)
    test_answer = models.CharField(max_length=255, null=True)
    test_date = models.DateTimeField(auto_now_add=True)
    test_result = models.URLField(blank=False)

    def __unicode__(self):
        return self.user.username
