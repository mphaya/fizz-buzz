# -*- coding: utf-8 -*-

from django import forms

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=50, required=True, help_text='Required.')
    last_name = forms.CharField(max_length=50, required=True, help_text="Required.")
    email = forms.EmailField(max_length=300, help_text="Required: Please enter a valid email address.")

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')


class UserFizzBuzzForm(forms.ModelForm):
    class Meta:
        model = UserFizzBuzz
        fields = ["test_answer"]